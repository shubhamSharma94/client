import { List, Avatar, Skeleton } from "antd";
import { DeleteOutlined } from "@ant-design/icons";
import { Popconfirm, message } from "antd";
import { useDispatch } from "react-redux";
import { deleteUser } from "../../../redux/actions/userAction";
import { showModal } from "../../../redux/actions/modalAction";

const ListItem = (props) => {
  const { name, phone, picture_thumbnail, id } = props;
  const dispatch = useDispatch();

  const confirm = (e) => {
    e.stopPropagation();
    dispatch(deleteUser(id));
    message.success(
      <span>
        You successfully deleted <b>{name}</b> from the list.
      </span>
    );
  };

  const cancel = (e) => {
    e.stopPropagation();
    message.success(
      <span>
        Thank god <b>{name}</b> is saved.
        <span>&#128514;</span>
      </span>
    );
  };
  return (
    <>
      <List.Item
        onClick={() => {
          dispatch(showModal(props));
        }}
        actions={[
          <Popconfirm
            title='Are you sure to delete this user?'
            onConfirm={confirm}
            onCancel={cancel}
            okText='Yes'
            cancelText='No'
          >
            <DeleteOutlined
              onClick={(e) => {
                e.stopPropagation();
              }}
            />
          </Popconfirm>,
        ]}
      >
        <Skeleton avatar title={false} loading={false} active>
          <List.Item.Meta
            avatar={<Avatar src={picture_thumbnail} />}
            title={<span>{name}</span>}
            description={phone}
          />
        </Skeleton>
      </List.Item>
    </>
  );
};

export default ListItem;
