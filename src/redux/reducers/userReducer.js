import {
  GET_USER_LIST_INITIATE,
  GET_USER_LIST_FAILED,
  GET_USER_LIST_SUCCESS,
  DELETE_ALL_USER_INITIATE,
  DELETE_ALL_USER_FAILED,
  DELETE_ALL_USER_SUCCESS,
  CREATE_NEW_USER_INITIATE,
  CREATE_NEW_USER_FAILED,
  CREATE_NEW_USER_SUCCESS,
  DELETE_USER_INITIATE,
  DELETE_USER_FAILED,
  DELETE_USER_SUCCESS,
} from "../constants/userConstants";

const initialData = {
  userList: [],
  status: "",
};

export const userReducer = (state = initialData, action = {}) => {
  switch (action.type) {
    case GET_USER_LIST_INITIATE:
      return Object.assign({}, state, {
        status: "waiting",
      });

    case GET_USER_LIST_SUCCESS:
      return Object.assign({}, state, {
        userList: action.payload,
        status: "success",
      });

    case GET_USER_LIST_FAILED:
      return Object.assign({}, state, {
        status: "failed",
      });

    case DELETE_ALL_USER_INITIATE:
      return Object.assign({}, state, {
        status: "waiting",
      });

    case DELETE_ALL_USER_SUCCESS:
      return Object.assign({}, state, {
        userList: [],
        status: "success",
      });

    case DELETE_ALL_USER_FAILED:
      return Object.assign({}, state, {
        status: "failed",
      });

    case CREATE_NEW_USER_INITIATE:
      return Object.assign({}, state, {
        status: "waiting",
      });

    case CREATE_NEW_USER_SUCCESS:
      return Object.assign({}, state, {
        userList: [...state.userList, ...action.payload],
        status: "success",
      });

    case CREATE_NEW_USER_FAILED:
      return Object.assign({}, state, {
        status: "failed",
      });

    case DELETE_USER_INITIATE:
      return Object.assign({}, state, {
        status: "waiting",
      });

    case DELETE_USER_SUCCESS:
      return Object.assign({}, state, {
        userList: state.userList.filter(function (obj) {
          return obj.id !== action.payload;
        }),
        status: "success",
      });

    case DELETE_USER_FAILED:
      return Object.assign({}, state, {
        status: "failed",
      });

    default:
      return state;
  }
};
