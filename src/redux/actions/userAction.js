import {
  GET_USER_LIST_INITIATE,
  GET_USER_LIST_FAILED,
  GET_USER_LIST_SUCCESS,
  DELETE_ALL_USER_INITIATE,
  DELETE_ALL_USER_FAILED,
  DELETE_ALL_USER_SUCCESS,
  CREATE_NEW_USER_INITIATE,
  CREATE_NEW_USER_FAILED,
  CREATE_NEW_USER_SUCCESS,
  DELETE_USER_INITIATE,
  DELETE_USER_FAILED,
  DELETE_USER_SUCCESS,
} from "../constants/userConstants";

import { apiHandler } from "../../utils/apiConfig";

export const getUserListInitiate = () => {
  return {
    type: GET_USER_LIST_INITIATE,
  };
};

export const getUserListFailed = () => {
  return {
    type: GET_USER_LIST_FAILED,
  };
};

export const getUserListSuccess = (data) => {
  return {
    type: GET_USER_LIST_SUCCESS,
    payload: data,
  };
};

export const getUserList = () => {
  return (dispatch) => {
    dispatch(getUserListInitiate());
    return getUserListApi()
      .then((response) => {
        if (response) {
          dispatch(getUserListSuccess(response.data));
        } else {
          dispatch(getUserListFailed());
        }
      })
      .catch((error) => {
        dispatch(getUserListFailed());
      });
  };
};

export const deleteAllUserInitiate = () => {
  return {
    type: DELETE_ALL_USER_INITIATE,
  };
};

export const deleteAllUserFailed = () => {
  return {
    type: DELETE_ALL_USER_FAILED,
  };
};

export const deleteAllUserSuccess = () => {
  return {
    type: DELETE_ALL_USER_SUCCESS,
  };
};

export const deleteAllUserList = () => {
  return (dispatch) => {
    dispatch(deleteAllUserInitiate());
    return deleteAllUserApi()
      .then((response) => {
        if (response.status === 204) {
          dispatch(deleteAllUserSuccess());
        } else {
          dispatch(deleteAllUserFailed());
        }
      })
      .catch((error) => {
        dispatch(deleteAllUserFailed());
      });
  };
};

export const createNewUserInitiate = () => {
  return {
    type: CREATE_NEW_USER_INITIATE,
  };
};

export const createNewUserFailed = () => {
  return {
    type: CREATE_NEW_USER_FAILED,
  };
};

export const createNewUserSuccess = (data) => {
  return {
    type: CREATE_NEW_USER_SUCCESS,
    payload: data,
  };
};

export const createNewUser = () => {
  return (dispatch) => {
    dispatch(createNewUserInitiate());
    return createNewUserApi()
      .then((response) => {
        if (response) {
          dispatch(createNewUserSuccess(response.data));
        } else {
          dispatch(createNewUserFailed());
        }
      })
      .catch((error) => {
        dispatch(createNewUserFailed());
      });
  };
};

export const deleteUserInitiate = () => {
  return {
    type: DELETE_USER_INITIATE,
  };
};

export const deleteUserFailed = () => {
  return {
    type: DELETE_USER_FAILED,
  };
};

export const deleteUserSuccess = (data) => {
  return {
    type: DELETE_USER_SUCCESS,
    payload: data,
  };
};

export const deleteUser = (id) => {
  return (dispatch) => {
    dispatch(deleteUserInitiate());
    return deleteUserApi(id)
      .then((response) => {
        if (response.status === 204) {
          dispatch(deleteUserSuccess(id));
        } else {
          dispatch(deleteUserFailed());
        }
      })
      .catch((error) => {
        dispatch(deleteUserFailed());
      });
  };
};

const getUserListApi = () => {
  return apiHandler
    .get(`/user`, {
      params: {}, // query param if required
    })
    .catch((err) => {
      console.log(err);
    });
};

const deleteAllUserApi = () => {
  return apiHandler
    .delete(`/user`, {
      params: {}, // query param if required
    })
    .catch((err) => {
      console.log(err);
    });
};

const createNewUserApi = () => {
  return apiHandler
    .post(`/user`, {
      params: {}, // query param if required
    })
    .catch((err) => {
      console.log(err);
    });
};

const deleteUserApi = (id) => {
  return apiHandler
    .delete(`/user/${id}`, {
      params: {}, // query param if required
    })
    .catch((err) => {
      console.log(err);
    });
};
